#include "stdafx.h"
#include "VideoRendererForWindows.h"

VideoRendererForWindows::VideoRendererForWindows(VideoRendererObserver *observer)
	:observer_(observer)
{
	::InitializeCriticalSection(&buffer_lock_);

	ZeroMemory(&bmi_, sizeof(bmi_));
	bmi_.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi_.bmiHeader.biPlanes = 1;
	bmi_.bmiHeader.biBitCount = 32;
	bmi_.bmiHeader.biCompression = BI_RGB;
#if 0
	bmi_.bmiHeader.biWidth = width;
	bmi_.bmiHeader.biHeight = -height;
	bmi_.bmiHeader.biSizeImage = width * height *
		(bmi_.bmiHeader.biBitCount >> 3);
#endif
}
VideoRendererForWindows::~VideoRendererForWindows()
{
	::DeleteCriticalSection(&buffer_lock_);
}

void VideoRendererForWindows::SetSize(int width, int height)
{
	AutoLock<VideoRendererForWindows> lock(this);

	if (width == bmi_.bmiHeader.biWidth && height == bmi_.bmiHeader.biHeight) {
		return;
	}

	bmi_.bmiHeader.biWidth = width;
	bmi_.bmiHeader.biHeight = height;
	bmi_.bmiHeader.biSizeImage = width * height *
		(bmi_.bmiHeader.biBitCount >> 3);
	image_.reset(new unsigned char[bmi_.bmiHeader.biSizeImage]);
}

void VideoRendererForWindows::RenderFrame(const char* buffer, unsigned int length)
{
	{
		AutoLock<VideoRendererForWindows> lock(this);
		memcpy(image_.get(), buffer, length);
	}

	if (observer_)
		observer_->OnVideoFrame();
}
