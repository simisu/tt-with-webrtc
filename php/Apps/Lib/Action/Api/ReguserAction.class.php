<?php

/*
 * 感谢使用微笑开发程序 如有不足 请多多指教
 * 默认是微笑独立开发的页面和程序 请不要修改发布到商业场景
 * 联系QQ 512720913  by.weixiao
 */

class ReguserAction extends Action {

    public function index() {
        header("Content-Type: text/html; charset=utf-8");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
 
        //接收post或者get数据 默认已经格式化输出了
        $sex = urldecode($this->_param('sex'));
        $name = urldecode($this->_param('name'));
        $nick = urldecode($this->_param('nick'));
        $password = $this->_param('password');
        $phone = $this->_param('phone');
        $email = urldecode($this->_param('email'));
        $avatar = $this->_param('avatar');
        $departId = $this->_param('departId');
        $sign = $this->_param('sign');
        
        //判断接口参数为空定义
        if (empty($sex)) {
            echo _tojson(1, "性别不能为空不能为空！", null);
            return;
        }
        if (empty($name)) {
            echo _tojson(1, "账户不能为空！", null);
            return;
        }
        if (empty($nick)) {
            echo _tojson(1, "用户昵称不能为空！", null);
            return;
        }
        if (empty($password)) {
            echo _tojson(1, "用户密码不能为空！", null);
            return;
        }
        if (empty($email)) {
            echo _tojson(1, "用户邮箱不能为空！", null);
            return;
        }
        if (empty($departId)) {
            echo _tojson(1, "部门ID不能为空！", null);
            return;
        }

        if (empty($sign)) {
            echo _tojson(1, "签名数据不能为空！", null);
            return;
        }

        // 开始判断签名 按照键名排序
        $prestr = $avatar . $departId . $email . $name . $nick . $password . $phone . $sex;
        if (md5Verify($prestr, $sign, C('sign_key'))) {
            //实例化用户数据数据表
            $User = M('User');
            //开始判断部门是否存在
            $return_user = $User->where(array("name" => $name))->find();

            if (empty($return_user)) {

                $salt = rand(1000, 9999);
                $arr = array(
                    "sex" => $sex,
                    "name" => $name,
                    "domain" => _toPinyin($nick),
                    "nick" => $nick,
                    "password" => md5(md5($password) . $salt),
                    "salt" => $salt,
                    "phone" => $phone,
                    "email" => $email,
                    "avatar" => $avatar,
                    "departId" => $departId,
                    "status" => "0",
                    "created" => time(),
                );
                $return_users = $User->add($arr);

                if ($return_users !== FALSE) {
                    echo _tojson(0, "ok", $code);
                } else {
                    echo _tojson(1, "写入数据库失败", $code);
                    return;
                }
            } else {
                echo _tojson(1, $departName . "已存在", null);
                return;
            }
        } else {
            echo _tojson("1", "数据签名校验失败！", C('check_sige_err'));
        }
    }

}
